/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.util;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

/**
 * Created by aston
 */
public final class TypeUtils {
    public static boolean isDeclaredType(final TypeMirror typeMirror, Class<?> declaredTypeClass) {
        return typeMirror.getKind() == TypeKind.DECLARED
                && ((TypeElement)((DeclaredType) typeMirror).asElement()).getQualifiedName().toString().equals(declaredTypeClass.getCanonicalName());
    }
}
