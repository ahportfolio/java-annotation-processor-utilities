/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.util;

import javax.annotation.processing.Filer;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

/**
 * Created by aston
 */
public class ProcessorToolkit {
    private Types mTypeUtils;
    private Elements mElementUtils;
    private Filer mFiler;
    private Logger mLogger;

    private int mProcessingRound = 0;

    public void incrementProcessingRound() {
        mProcessingRound++;
    }

    public int getProcessingRound() {
        return mProcessingRound;
    }

    public Types getTypeUtils() {
        return mTypeUtils;
    }

    public void setTypeUtils(final Types typeUtils) {
        mTypeUtils = typeUtils;
    }

    public Elements getElementUtils() {
        return mElementUtils;
    }

    public void setElementUtils(final Elements elementUtils) {
        mElementUtils = elementUtils;
    }

    public Filer getFiler() {
        return mFiler;
    }

    public void setFiler(final Filer filer) {
        mFiler = filer;
    }

    public Logger getLogger() {
        return mLogger;
    }

    public void setLogger(final Logger logger) {
        mLogger = logger;
    }
}
