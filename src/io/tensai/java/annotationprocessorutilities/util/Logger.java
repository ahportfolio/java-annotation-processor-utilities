/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.util;

import io.tensai.java.annotationprocessorutilities.interfaces.processor.IProcessorLogger;

import javax.annotation.processing.Messager;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

/**
 * Created by aston
 */
public class Logger implements IProcessorLogger {
    private final Messager mMessager;

    public Logger(final Messager messager) {
        mMessager = messager;
    }

    @Override
    public void info(final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.NOTE, String.format(message, args));
    }

    @Override
    public void error(final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.ERROR, String.format(message, args));
    }

    @Override
    public void warn(final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.WARNING, String.format(message, args));
    }

    @Override
    public void debug(final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.OTHER, String.format(message, args));
    }

    @Override
    public void info(final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.NOTE, String.format(message, args), e);
    }

    @Override
    public void error(final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.ERROR, String.format(message, args), e);
    }

    @Override
    public void warn(final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.WARNING, String.format(message, args), e);
    }

    @Override
    public void debug(final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.OTHER, String.format(message, args), e);
    }

    @Override
    public void info(final AnnotationMirror a, final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.NOTE, String.format(message, args), e, a);
    }

    @Override
    public void error(final AnnotationMirror a, final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.ERROR, String.format(message, args), e, a);
    }

    @Override
    public void warn(final AnnotationMirror a, final Element e, final String message, final Object... args) {

    }

    @Override
    public void debug(final AnnotationMirror a, final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.OTHER, String.format(message, args), e, a);
    }

    @Override
    public void info(final AnnotationValue v, final AnnotationMirror a, final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.NOTE, String.format(message, args), e, a, v);
    }

    @Override
    public void error(final AnnotationValue v, final AnnotationMirror a, final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.ERROR, String.format(message, args), e, a, v);
    }

    @Override
    public void warn(final AnnotationValue v, final AnnotationMirror a, final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.WARNING, String.format(message, args), e, a, v);
    }

    @Override
    public void debug(final AnnotationValue v, final AnnotationMirror a, final Element e, final String message, final Object... args) {
        mMessager.printMessage(Diagnostic.Kind.OTHER, String.format(message, args), e, a, v);
    }
}
