/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.wrappers;

import javax.lang.model.element.*;
import javax.lang.model.util.ElementFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aston
 */
public class ClassElementWrapper extends ElementWrapperFactory.IElementWrapperImpl {

    private ClassFieldElementWrapper[] mFieldsCache;
    private ClassMethodElementWrapper[] mMethodsCache;

    public ClassElementWrapper(final TypeElement element) {
        super(element);
    }

    @Override
    public WrapperType getWrapperType() {
        return WrapperType.Class;
    }

    public ClassFieldElementWrapper[] getFields() {
        if (mFieldsCache == null) {
            final List<ClassFieldElementWrapper> fieldElementWrappers = new ArrayList<>();

            for (final Element element : ElementFilter.fieldsIn(getSourceElement().getEnclosedElements())) {
                if (element.getKind() == ElementKind.FIELD) {
                    fieldElementWrappers.add(new ClassFieldElementWrapper((VariableElement) element));
                }
            }
            mFieldsCache = fieldElementWrappers.toArray(new ClassFieldElementWrapper[fieldElementWrappers.size()]);
        }

        return mFieldsCache;
    }

    public ClassMethodElementWrapper[] getMethods() {
        if (mMethodsCache == null) {
            final List<ClassMethodElementWrapper> methodElementWrappers = new ArrayList<>();

            for (final Element element : getSourceElement().getEnclosedElements()) {
                if (element.getKind() == ElementKind.METHOD) {
                    methodElementWrappers.add(new ClassMethodElementWrapper((ExecutableElement) element));
                }
            }
            mMethodsCache = methodElementWrappers.toArray(new ClassMethodElementWrapper[methodElementWrappers.size()]);
        }

        return mMethodsCache;
    }

    @Override
    public TypeElement getSourceElement() {
        return (TypeElement) super.getSourceElement();
    }

    public String getQualifiedName() {
        return getSourceElement().getQualifiedName().toString();
    }
    public String getPackageName() {
        final String qualifiedName = getSourceElement().getQualifiedName().toString();
        final int lastDotIndex = qualifiedName.lastIndexOf('.');

        return lastDotIndex > -1 ? qualifiedName.substring(0, lastDotIndex) : "";
    }
}
