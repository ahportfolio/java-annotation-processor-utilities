/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.wrappers;

import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import java.lang.annotation.Annotation;

/**
 * Created by aston
 */
public class ClassFieldElementWrapper extends ElementWrapperFactory.IElementWrapperImpl {
    public ClassFieldElementWrapper(final VariableElement element) {
        super(element);
    }

    public boolean hasAnnotation(final Class<? extends Annotation> ann) {
        return getSourceElement().getAnnotation(ann) != null;
    }

    public <A extends Annotation> A getAnnotation(final Class<A> ann) {
        return getSourceElement().getAnnotation(ann);
    }

    public TypeKind getVariableType() {
        return getSourceElement().asType().getKind();
    }

    @Override
    public WrapperType getWrapperType() {
        return WrapperType.Field;
    }

    @Override
    public VariableElement getSourceElement() {
        return (VariableElement) super.getSourceElement();
    }
}
