/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.wrappers;

import io.tensai.java.annotationprocessorutilities.exceptions.ProcessorException;
import io.tensai.java.annotationprocessorutilities.interfaces.wrappers.IElementWrapper;

import javax.lang.model.element.*;
import java.lang.annotation.Annotation;

/**
 * Created by aston
 */
public final class ElementWrapperFactory {
    public static IElementWrapper buildTypeFromElement(final Element element) {
        switch (element.getKind()) {
            case CLASS:
                return new ClassElementWrapper((TypeElement) element);
            case FIELD:
                return new ClassFieldElementWrapper((VariableElement) element);
            case METHOD:
                return new ClassMethodElementWrapper((ExecutableElement) element);
        }

        throw new ProcessorException("Tried to wrap unhandled element kind: " + element.getKind().name());
    }

    protected abstract static class IElementWrapperImpl implements IElementWrapper {
        private final Element mElement;

        public IElementWrapperImpl(final Element element) {
            mElement = element;
        }

        @Override
        public Element getSourceElement() {
            return mElement;
        }

        public String getSimpleName() {
            return mElement.getSimpleName().toString();
        }
        public <A extends Annotation> A getAnnotation(final Class<A> annotationClass) {
            return mElement.getAnnotation(annotationClass);
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            final IElementWrapperImpl that = (IElementWrapperImpl) o;

            return mElement != null ? mElement.equals(that.mElement) : that.mElement == null;

        }

        @Override
        public int hashCode() {
            return mElement != null ? mElement.hashCode() : 0;
        }
    }
}
