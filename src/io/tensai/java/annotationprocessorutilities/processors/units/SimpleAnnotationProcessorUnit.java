/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.processors.units;

import io.tensai.java.annotationprocessorutilities.interfaces.processor.IProcessorUnit;
import io.tensai.java.annotationprocessorutilities.interfaces.wrappers.IElementWrapper;
import io.tensai.java.annotationprocessorutilities.util.ProcessorToolkit;
import io.tensai.java.annotationprocessorutilities.wrappers.ClassElementWrapper;
import io.tensai.java.annotationprocessorutilities.wrappers.ElementWrapperFactory;
import io.tensai.java.annotationprocessorutilities.wrappers.ClassFieldElementWrapper;
import io.tensai.java.annotationprocessorutilities.wrappers.ClassMethodElementWrapper;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by aston
 */
public class SimpleAnnotationProcessorUnit implements IProcessorUnit {

    private final AnnotationQuery[] mQueries;

    public SimpleAnnotationProcessorUnit(final AnnotationQuery... queries) {
        mQueries = queries;
    }

    @Override
    public List<String> getSupportedAnnotations() {
        final List<String> annotations = new ArrayList<>(mQueries.length);
        for (final AnnotationQuery query : mQueries) {
            annotations.add(query.getAnnotation().getCanonicalName());
        }
        return annotations;
    }

    @Override
    public void processEnvironment(final RoundEnvironment env, final ProcessorToolkit toolkit) {
        for (final AnnotationQuery query : mQueries) {
            final List<IElementWrapper> foundElements = new ArrayList<>();
            for (final Element element : env.getElementsAnnotatedWith(query.getAnnotation())) {
                if (query.isElementAccepted(element)) {
                    final IElementWrapper wrappedElement = ElementWrapperFactory.buildTypeFromElement(element);
                    foundElements.add(wrappedElement);
                    onElementFound(wrappedElement, toolkit);

                    switch (wrappedElement.getWrapperType()) {
                        case Class:
                            onClassElementFound((ClassElementWrapper) wrappedElement, toolkit);
                            break;
                        case Field:
                            onFieldElementFound((ClassFieldElementWrapper) wrappedElement, toolkit);
                            break;
                        case Method:
                            onMetohdElementFound((ClassMethodElementWrapper) wrappedElement, toolkit);
                            break;
                    }
                }
            }

            onElementsFound(foundElements, toolkit);
        }
    }

    protected void onClassElementFound(final ClassElementWrapper element, final ProcessorToolkit toolkit) {
        // do nothing
    }

    protected void onFieldElementFound(final ClassFieldElementWrapper element, final ProcessorToolkit toolkit) {
        // do nothing
    }

    protected void onMetohdElementFound(final ClassMethodElementWrapper element, final ProcessorToolkit toolkit) {
        // do nothing
    }

    protected void onElementFound(final IElementWrapper element, final ProcessorToolkit toolkit) {
        // do nothing
    }

    protected void onElementsFound(final List<IElementWrapper> elements, final ProcessorToolkit toolkit) {
        // do nothing
    }

    public static class AnnotationQuery {
        private final Class<? extends Annotation> mAnnotation;
        private final IElementWrapper.WrapperType[] mAcceptedTypes;

        public AnnotationQuery(final Class<? extends Annotation> annotation, final IElementWrapper.WrapperType... acceptedTypes) {
            mAnnotation = annotation;
            mAcceptedTypes = acceptedTypes;
            Arrays.sort(mAcceptedTypes);
        }

        public Class<? extends Annotation> getAnnotation() {
            return mAnnotation;
        }

        public boolean isElementAccepted(final Element element) {
            switch (element.getKind()) {
                case CLASS:
                    return Arrays.binarySearch(mAcceptedTypes, IElementWrapper.WrapperType.Class) > -1;
                case FIELD:
                    return Arrays.binarySearch(mAcceptedTypes, IElementWrapper.WrapperType.Field) > -1;
                case METHOD:
                    return Arrays.binarySearch(mAcceptedTypes, IElementWrapper.WrapperType.Method) > -1;
            }

            return false;
        }

        protected Element mapElement(final Element element) {
            return element;
        }
    }
}
