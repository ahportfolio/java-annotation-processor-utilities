/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.processors;

import io.tensai.java.annotationprocessorutilities.exceptions.ProcessorException;
import io.tensai.java.annotationprocessorutilities.exceptions.SourceAnnotationException;
import io.tensai.java.annotationprocessorutilities.exceptions.SourceAnnotationValueException;
import io.tensai.java.annotationprocessorutilities.exceptions.SourceElementException;
import io.tensai.java.annotationprocessorutilities.interfaces.processor.IProcessorUnit;
import io.tensai.java.annotationprocessorutilities.util.Logger;
import io.tensai.java.annotationprocessorutilities.util.ProcessorToolkit;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by aston
 */
public abstract class AbstractComposableAnnotationProcessor extends AbstractProcessor {
    private final IProcessorUnit[] mProcessorUnits;
    public static ProcessorToolkit mToolkit;

    private boolean isDebugging;

    protected AbstractComposableAnnotationProcessor(final IProcessorUnit... processorUnits) {
        super();
        mProcessorUnits = processorUnits;
        mToolkit = new ProcessorToolkit();
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);

        mToolkit.setTypeUtils(processingEnv.getTypeUtils());;
        mToolkit.setElementUtils(processingEnv.getElementUtils());
        mToolkit.setFiler(processingEnv.getFiler());
        mToolkit.setLogger(new Logger(processingEnv.getMessager()));

        mToolkit.getLogger().debug("Initializing processor: " + getClass().getSimpleName());
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> annotations = new HashSet<String>();

        for (final IProcessorUnit processorUnit : mProcessorUnits) {
            annotations.addAll(processorUnit.getSupportedAnnotations());
        }

        return annotations;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public boolean process(final Set<? extends TypeElement> annotations, final RoundEnvironment roundEnv) {
        mToolkit.incrementProcessingRound();

        try {
            mToolkit.getLogger().debug("Begin processing: " + getClass().getSimpleName());

            for (final IProcessorUnit processorUnit : mProcessorUnits) {
                processorUnit.processEnvironment(roundEnv, mToolkit);
            }

        } catch (final SourceAnnotationValueException e) {
            mToolkit.getLogger().error(e.getMessage(), e.getElement(), e.getAnnotation(), e.getValue());
        } catch (final SourceAnnotationException e) {
            mToolkit.getLogger().error(e.getMessage(), e.getElement(), e.getAnnotation());
        } catch (final SourceElementException e) {
            mToolkit.getLogger().error(e.getMessage(), e.getElement());
        } catch (final ProcessorException e) {
            mToolkit.getLogger().error(e.getMessage());
        }

        mToolkit.getLogger().debug("End processing: " + getClass().getSimpleName());

        if (isDebugging) {
            mToolkit.getLogger().error("Debugging processor: " + getClass().getSimpleName());
            return false;
        } else {
            return true;
        }
    }

    public boolean isDebuggingEnabled() {
        return isDebugging;
    }

    public void enableDebugging() {
        isDebugging = true;
    }

    public void disableDebugging() {
        isDebugging = false;
    }
}
