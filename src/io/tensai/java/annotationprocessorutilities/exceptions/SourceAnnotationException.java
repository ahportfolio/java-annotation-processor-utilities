/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.exceptions;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;

/**
 * Created by aston
 */
public class SourceAnnotationException extends SourceElementException {
    private final AnnotationMirror mAnnotation;
    public SourceAnnotationException(final AnnotationMirror annotation, final Element element) {
        super(element);
        mAnnotation = annotation;
    }

    public SourceAnnotationException(final AnnotationMirror annotation, final Element element, final String message) {
        super(element, message);
        mAnnotation = annotation;
    }

    public SourceAnnotationException(final AnnotationMirror annotation, final Element element, final String message, final Throwable cause) {
        super(element, message, cause);
        mAnnotation = annotation;
    }

    public SourceAnnotationException(final AnnotationMirror annotation, final Element element, final Throwable cause) {
        super(element, cause);
        mAnnotation = annotation;
    }

    public SourceAnnotationException(final AnnotationMirror annotation, final Element element, final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(element, message, cause, enableSuppression, writableStackTrace);
        mAnnotation = annotation;
    }

    public AnnotationMirror getAnnotation() {
        return mAnnotation;
    }
}
