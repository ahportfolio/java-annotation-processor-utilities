/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.exceptions;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;

/**
 * Created by aston
 */
public class SourceAnnotationValueException extends SourceAnnotationException {
    private final AnnotationValue mValue;
    public SourceAnnotationValueException(final AnnotationValue value, final AnnotationMirror annotation, final Element element) {
        super(annotation, element);
        mValue = value;
    }

    public SourceAnnotationValueException(final AnnotationValue value, final AnnotationMirror annotation, final Element element, final String message) {
        super(annotation, element, message);
        mValue = value;
    }

    public SourceAnnotationValueException(final AnnotationValue value, final AnnotationMirror annotation, final Element element, final String message, final Throwable cause) {
        super(annotation, element, message, cause);
        mValue = value;
    }

    public SourceAnnotationValueException(final AnnotationValue value, final AnnotationMirror annotation, final Element element, final Throwable cause) {
        super(annotation, element, cause);
        mValue = value;
    }

    public SourceAnnotationValueException(final AnnotationValue value, final AnnotationMirror annotation, final Element element, final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(annotation, element, message, cause, enableSuppression, writableStackTrace);
        mValue = value;
    }

    public AnnotationValue getValue() {
        return mValue;
    }
}
