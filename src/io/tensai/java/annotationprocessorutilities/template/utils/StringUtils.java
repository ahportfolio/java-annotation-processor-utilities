/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.template.utils;

/**
 * Created by aston
 */
public final class StringUtils {
    private static StringUtils INSTANCE = null;

    public static StringUtils getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new StringUtils();
        }

        return INSTANCE;
    }

    public String format(final String formatString, Object... args) {
        return String.format(formatString, args);
    }

    public String camel_to_snake_case(final String camelCase) {
        final StringBuilder builder;
        int i, capitalCharacterCount = 0;
        char previousCharacter;

        for (i = 0; i < camelCase.length(); i++) {
            if (Character.isUpperCase(camelCase.charAt(i))) {
                capitalCharacterCount++;
            }
        }

        previousCharacter = '_';
        builder = new StringBuilder(camelCase.length() + capitalCharacterCount);
        for (i = 0; i < camelCase.length(); i++) {
            char currentCharacter = camelCase.charAt(i);
            if (Character.isUpperCase(currentCharacter) && Character.isLowerCase(previousCharacter)) {
                builder.append('_').append(currentCharacter);
            } else {
                builder.append(currentCharacter);
            }

            previousCharacter = currentCharacter;
        }

        return builder.toString();
    }

    public String snake_to_camel_case(final String snakeCase) {
        final String[] parts = snakeCase.split("_");
        final StringBuilder builder = new StringBuilder(snakeCase.length());


        for (int i = 0, partsLength = parts.length; i < partsLength; i++) {
            final String part = parts[i];
            if (i != 0) {
                builder.append(Character.toUpperCase(part.charAt(0)));
                if (part.length() > 1) {
                    builder.append(part.substring(1));
                }
            } else {
                builder.append(part);
            }
        }

        return builder.toString();
    }
}
