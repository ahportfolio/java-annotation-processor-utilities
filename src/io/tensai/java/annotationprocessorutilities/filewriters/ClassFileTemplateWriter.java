/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.filewriters;

import freemarker.cache.*;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by aston
 */
public class ClassFileTemplateWriter extends AbstractClassFileWriter {
    private static final Configuration CONFIG_TEMPLATE_ENGINE = new Configuration(Configuration.VERSION_2_3_23);
    private static FileTemplateLoader TEMPLATE_LOADER_FILE = null;
    private static ClassTemplateLoader TEMPLATE_LOADER_CLASS = null;
    private static final StringTemplateLoader TEMPLATE_LOADER_STRING = new StringTemplateLoader();
    private static final MultiTemplateLoader TEMPLATE_LOADER_MULTI = new MultiTemplateLoader(new TemplateLoader[]{
            TEMPLATE_LOADER_STRING
    });

    private static int COUNT_FRAGMENT = 0;

    static {
        CONFIG_TEMPLATE_ENGINE.setDefaultEncoding("UTF-8");
        CONFIG_TEMPLATE_ENGINE.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        CONFIG_TEMPLATE_ENGINE.setTemplateLoader(TEMPLATE_LOADER_MULTI);
    }

    private static final ScopeMarker SCOPE_MARKER_DEFAULT = new ScopeMarker("//>", "//begin>", "//end>");
    private static File FILE_DIRECTORY_TEMPLATE_ROOT = null;
    private static Class CLASS_TEMPLATE_ROOT = null;

    private final String mTemplateFilePath;
    private final Map<String, ?> mTemplateEnvironment;
    private final boolean mOnlyScopedLines;
    private final ScopeMarker mScopeMarker;

    public ClassFileTemplateWriter(final String fullyQualifiedClassName, final String templateFilePath, final Map<String, ?> templateEnvironment) {
        this(fullyQualifiedClassName, templateFilePath, templateEnvironment, true);
    }

    public ClassFileTemplateWriter(final String fullyQualifiedClassName, final String templateFilePath, final Map<String, ?> templateEnvironment, final boolean onlyMarkedLines) {
        this(fullyQualifiedClassName, templateFilePath, templateEnvironment, onlyMarkedLines, SCOPE_MARKER_DEFAULT);
    }

    public ClassFileTemplateWriter(final String fullyClassQualifiedName, final String templateFilePath, final Map<String, ?> templateEnvironment, final boolean onlyMarkedLines, final ScopeMarker scopeMarker) {
        super(fullyClassQualifiedName);

        mTemplateFilePath = templateFilePath;
        mTemplateEnvironment = templateEnvironment;
        mOnlyScopedLines = onlyMarkedLines;
        mScopeMarker = scopeMarker;
    }

    @Override
    protected void writeContents(final Writer writer) throws IOException {
        try {
            if (mOnlyScopedLines) {
                final InputStream templateFileStream;

                if (CLASS_TEMPLATE_ROOT != null) {
                    templateFileStream = CLASS_TEMPLATE_ROOT.getResourceAsStream(mTemplateFilePath);
                    if (templateFileStream == null) {
                        throw new FileNotFoundException("The class loader was not able to resolve the template url: " + mTemplateFilePath);
                    }
                } else if (FILE_DIRECTORY_TEMPLATE_ROOT != null) {
                    templateFileStream = new FileInputStream(new File(FILE_DIRECTORY_TEMPLATE_ROOT, mTemplateFilePath));
                } else {
                    templateFileStream = new FileInputStream(mTemplateFilePath);
                }

                try (final BufferedReader reader = new BufferedReader(new InputStreamReader(templateFileStream))) {
                    String line;
                    boolean isInBlock = false;
                    StringBuilder blockStringBuilder = null;

                    while ((line = reader.readLine()) != null) {
                        if (isInBlock) {
                            if (line.startsWith(mScopeMarker.getBlockEndMarker())) {
                                isInBlock = false;
                                writer.write(getProcessedTemplateString(mTemplateFilePath, blockStringBuilder.toString(), mTemplateEnvironment));
                                writer.write('\n');
                                blockStringBuilder.setLength(0);
                            } else {
                                blockStringBuilder.append(line);
                                blockStringBuilder.append('\n');
                            }
                        } else {
                            if (line.startsWith(mScopeMarker.getLineMarker())) {
                                writer.write(getProcessedTemplateString(mTemplateFilePath, line.substring(mScopeMarker.getLineMarker().length()), mTemplateEnvironment));
                                writer.write('\n');
                            } else if (line.startsWith(mScopeMarker.getBlockStartMarker())) {
                                if (blockStringBuilder == null) {
                                    blockStringBuilder = new StringBuilder();
                                }
                                isInBlock = true;
                            } else {
                                writer.write(line);
                                writer.write('\n');
                            }
                        }
                    }
                }
            } else {
                CONFIG_TEMPLATE_ENGINE.getTemplate(mTemplateFilePath).process(mTemplateEnvironment, writer);
            }
        } catch (TemplateException e) {
            throw new IOException("An error occurred while processing the template: " + mTemplateFilePath + " " + e.getCause() + " " + e.toString() + " " + Arrays.toString(e.getStackTrace()), e);
        }
    }

    public static String getProcessedTemplateString(final String templateNameHint, final String sourceString, final Map templateEnvironment) throws TemplateException {
        final String templateName = "anon:" + templateNameHint + ":templateFragment:" + (COUNT_FRAGMENT++);
        final StringWriter sw = new StringWriter();
        TEMPLATE_LOADER_STRING.putTemplate(templateName, sourceString);
        try {
            CONFIG_TEMPLATE_ENGINE.getTemplate(templateName).process(templateEnvironment, sw);
        } catch (IOException e) {
            throw new TemplateException("An unexpected error has occurred when building the template object: " + e.toString(), e, null);
        }
        return sw.toString();
    }

    public static void setTemplateRootFolder(final String path) throws IOException {
        if (path == null) {
            FILE_DIRECTORY_TEMPLATE_ROOT = null;
            TEMPLATE_LOADER_FILE = null;
        } else {
            final File f = new File(path);

            FILE_DIRECTORY_TEMPLATE_ROOT = f;
            TEMPLATE_LOADER_FILE = new FileTemplateLoader(f);
        }

        CONFIG_TEMPLATE_ENGINE.setTemplateLoader(buildFreemakerTemplateLoader());
    }

    public static void setTemplateResourceLoaderClass(final Class clazz) {
        if (clazz == null) {
            TEMPLATE_LOADER_CLASS = null;
            CLASS_TEMPLATE_ROOT = null;
        } else {
            TEMPLATE_LOADER_CLASS = new ClassTemplateLoader(clazz, "");
            CLASS_TEMPLATE_ROOT = clazz;
        }

        CONFIG_TEMPLATE_ENGINE.setTemplateLoader(buildFreemakerTemplateLoader());
    }

    public static void setDefaultTemplateEncoding(final String encoding) {
        CONFIG_TEMPLATE_ENGINE.setDefaultEncoding(encoding);
    }

    private static TemplateLoader buildFreemakerTemplateLoader() {
        final ArrayList<TemplateLoader> loaders = new ArrayList<>(3);
        loaders.add(TEMPLATE_LOADER_STRING);

        if (TEMPLATE_LOADER_FILE != null) {
            loaders.add(TEMPLATE_LOADER_FILE);
        }

        if (TEMPLATE_LOADER_CLASS != null) {
            loaders.add(TEMPLATE_LOADER_CLASS);
        }

        return new MultiTemplateLoader(loaders.toArray(new TemplateLoader[loaders.size()]));
    }

    public static class ScopeMarker {
        private final String mLineMarker;
        private final String mBlockStartMarker;
        private final String mBlockEndMarker;

        public ScopeMarker(final String lineMarker, final String blockStartMarker, final String blockEndMarker) {
            mLineMarker = lineMarker;
            mBlockStartMarker = blockStartMarker;
            mBlockEndMarker = blockEndMarker;
        }

        public String getLineMarker() {
            return mLineMarker;
        }

        public String getBlockStartMarker() {
            return mBlockStartMarker;
        }

        public String getBlockEndMarker() {
            return mBlockEndMarker;
        }
    }
}
