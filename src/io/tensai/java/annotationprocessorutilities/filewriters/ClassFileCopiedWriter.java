/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.filewriters;

import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by aston
 */
public class ClassFileCopiedWriter extends AbstractClassFileWriter {
    private final String mSourceFileName;

    public ClassFileCopiedWriter(final String fullyQualifiedClassName, final String sourceFileName) {
        super(fullyQualifiedClassName);
        mSourceFileName = sourceFileName;
    }

    @Override
    protected void writeContents(final Writer writer) throws IOException {
        final FileReader fr = new FileReader(mSourceFileName);
        int character;
        while((character = fr.read()) != -1) {
            writer.write(character);
        }
    }
}
