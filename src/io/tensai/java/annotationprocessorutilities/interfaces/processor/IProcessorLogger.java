/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.interfaces.processor;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;

/**
 * Created by aston
 */
public interface IProcessorLogger {
    void info(final String message, final Object...args);
    void error(final String message, final Object...args);
    void warn(final String message, final Object...args);
    void debug(final String message, final Object...args);

    void info(final Element e, final String message, final Object...args);
    void error(final Element e, final String message, final Object...args);
    void warn(final Element e, final String message, final Object...args);
    void debug(final Element e, final String message, final Object...args);

    void info(final AnnotationMirror a, final Element e, final String message, final Object...args);
    void error(final AnnotationMirror a, final Element e, final String message, final Object...args);
    void warn(final AnnotationMirror a, final Element e, final String message, final Object...args);
    void debug(final AnnotationMirror a, final Element e, final String message, final Object...args);

    void info(final AnnotationValue v, final AnnotationMirror a, final Element e, final String message, final Object...args);
    void error(final AnnotationValue v, final AnnotationMirror a, final Element e, final String message, final Object...args);
    void warn(final AnnotationValue v, final AnnotationMirror a, final Element e, final String message, final Object...args);
    void debug(final AnnotationValue v, final AnnotationMirror a, final Element e, final String message, final Object...args);
}
