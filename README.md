# java-annotation-processor-utilities

Annotation processing enables a new breed of application libraries that provide
significant opportunities to optimize our development workflows, while having little
to no impact on runtime performance, by generating optimized code from sufficiently
terse APIs -- without leaving the standard JAVA/Android toolchain. Although Annotation
Processing can be very powerful, it's API is a relatively low level and arguably difficult
to debug. This library is a suite of APIs built to manipulate the annotation processor
primitives, to make the implementation of specific processors a little easier and less
prone to the general errors that I commonly encounter when building annotation processors.

## How to install

This library is distributed as a single JAR that is meant ot be a dependency of specific
 annotation processor implementations

## How to use

Most of this library contains Utility classes that can be used as necessary to aid the
implementation of specific annotation processors. You can take a look at the packages
and classes in the `io.tensai.java.annotationprocessorutilities` package to see the
functionality available.

## License

LGPL-v3
