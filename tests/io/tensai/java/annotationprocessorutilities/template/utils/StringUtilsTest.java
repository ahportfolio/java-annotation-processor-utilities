/*
 *     Copyright (C) 2016 Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.java.annotationprocessorutilities.template.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by aston
 */
public class StringUtilsTest {

    @Test
    public void testCamelToSnakeCase() throws Exception {
        final StringUtils stringUtils = new StringUtils();
        Assert.assertEquals("Test_Name", stringUtils.camel_to_snake_case("TestName"));
        Assert.assertEquals("Test_Name_Other", stringUtils.camel_to_snake_case("TestNameOther"));
        Assert.assertEquals("NEXTTest_Name_Other", stringUtils.camel_to_snake_case("NEXTTestNameOther"));
        Assert.assertEquals("NEXTTest_Name_NEWOther", stringUtils.camel_to_snake_case("NEXTTestNameNEWOther"));
        Assert.assertEquals("m_Name_NEWOther", stringUtils.camel_to_snake_case("m_NameNEWOther"));
    }
}